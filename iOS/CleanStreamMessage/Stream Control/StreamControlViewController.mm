//
//  StreamControlViewController.m
//  Encoder Demo


#import "StreamControlViewController.h"
#import "CameraServer.h"

@implementation StreamControlViewController

@synthesize cameraView;
@synthesize streamAddress;

//Set up for direction controls
float vel;
float turnAngle;

//cv::Rect boundingBox;
int boxCoords[2] = {0, 0};
float averageBoxSize = 0;

- (void)viewDidLoad
{
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    [super viewDidLoad];
    [self startPreview];
    
    //Double Robot
    NSLog(@"SDK Version: %@", kDoubleBasicSDKVersion);
    [DRDouble sharedDouble].delegate = self;
    
    //Default direction off
    vel = 0.0f;
    turnAngle = 0.0f;
    
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    // this is not the most beautiful animation...
    AVCaptureVideoPreviewLayer* preview = [[CameraServer server] getPreviewLayer];
    preview.frame = self.cameraView.bounds;
    //[[preview connection] setVideoOrientation:toInterfaceOrientation]; - we do not really want it to rotate..
}

- (void) startPreview
{
    AVCaptureVideoPreviewLayer* preview = [[CameraServer server] getPreviewLayer];
    [preview removeFromSuperlayer];
    preview.frame = self.cameraView.bounds;
    //[[preview connection] setVideoOrientation:UIInterfaceOrientationPortrait]; - we do not really want it to rotate..
    
    [self.cameraView.layer addSublayer:preview];
    
    self.streamAddress.text = [[CameraServer server] getURL];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//--        Double Robotics     --//


#pragma mark - DRDoubleDelegate
- (void)doubleDidConnect:(DRDouble *)theDouble {
    _DRConnectionStatus.text = @"Connected";
    [theDouble retractKickstands];
}

-(void)doubleDidDisconnect:(DRDouble *)theDouble {
    _DRConnectionStatus.text = @"Not Connected";
}

-(void)doubleDriveShouldUpdate:(DRDouble *)theDouble {
    
    if(!_rotationSwitch.isOn)
    {
        turnAngle = 0.0;
    }

    if (!_DriveSwitch.isOn){
        vel = 0.0;
    }

    if(!_kickStandSwitch.isOn) {
        turnAngle = 0.0;
        vel = 0.0;
    } else {
        [theDouble retractKickstands];
    }

    [theDouble variableDrive:vel turn:turnAngle];
}

- (void)doubleStatusDidUpdate:(DRDouble *)theDouble {
    _batteryPercentLabel.text = [NSString stringWithFormat:@"%f", [DRDouble sharedDouble].batteryPercent];
}

//--        COMMUNICATION       --//


//Method created to start a network session
- (void)initNetworkCommunication {
    
    //Test Log
    NSLog(@"This begins the network Communication");
    NSLog(@"Kickstand status", _rotationSwitch.isOn);
    NSLog(@"Kickstand status", _DriveSwitch.isOn);
    
    //Initialise a read and write stream
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    
  //  int val = [_serverPort.text int];
    
    //Bind 2 streams to a host and port, to allow freecasting of CFStreams to NSStreams
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)(_serverAddress.text), 5009, &readStream, &writeStream);
    //CFStreamCreatePairWithSocketToHost
    //set the input and output streams to read and write
    inputStream = (__bridge NSInputStream *)readStream;
    outputStream = (__bridge NSOutputStream *)writeStream;
    
    
    //set their delegates to themselves
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    
    //Allow the streams to be ready to send and recieve data at any time in a run loop
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    
    //Open the connection, (Actually Establish it)
    [inputStream open];
    [outputStream open];
    
    
    //Change button to connected
    [self setbuttonconnected];
    
    //enable sending information to server
    [self enableSending];
    
    
    //call method to send message to server
    [self SendMessage: @"Request Connection"];
    
}

- (void)SendMessage: (NSString *)message{
    
    //Sends a message over wifi
    NSString *response = [NSString stringWithFormat: @"%@",message];
    
    NSData *data = [[NSData alloc] initWithData:[response dataUsingEncoding:NSASCIIStringEncoding]];
    
    //[outputStream write: [data bytes] maxLength: [data length]]; --TODO: THIS CAUSES ERRORS WITH C++ COMPLIER ADDED
}

-(void)stream: (NSStream *)aStream handleEvent:(NSStreamEvent)eventCode{
    
    //Check status of connection
    switch(eventCode) {
            
        case NSStreamEventOpenCompleted:
            NSLog(@"Stream Opened");
            
        case NSStreamEventHasBytesAvailable: //Core Networking. This is where It sends the messsage
            if (aStream == inputStream)
            {
                //set buffer size for message. This is how big of a packet gets sent over the network.
                uint8_t buffer[1024];
                long len;
                
                //Check if inputstream has a message to be read
                while([inputStream hasBytesAvailable])
                {
                    //Attempt to decrypt and read it in
                    len = [inputStream read:buffer maxLength:sizeof(buffer)];
                    if (len > 0)
                    {
                        NSString *input = [[NSString alloc] initWithBytes: buffer length: len encoding: NSASCIIStringEncoding];
                        
                        if (nil != input)
                        {
                            //print the information
                            NSLog(@"Server Said: %@", input);
                            
                            //set the top left dialogue box to connected if its the first message sent
                            if ([input  isEqual: @"connected"])
                            {
                                [self displayMessage: input];
                            }
                            else [self setIncomeText: input]; //set the main text box text to the sensor data
                            
                            //NSRange range = [input rangeOfString:@"/" options:NSBackwardsSearch];
                            //mmesage terminator -d=+1.0-t=-0.0, /
                            
                            for (unsigned int i=0; i < [input length]; ++i)
                            {
                                if ([input characterAtIndex:i] == '-')
                                {
                                    ++i;
                                    if ([input characterAtIndex:i] == 'd' )
                                    {
                                        i += 2;
                                        NSRange range;
                                        range.location = i;
                                        range.length = 3;
                                
                                        
                                        NSString *substring = [input substringWithRange:range];
                                        NSLog(@"drive read as %@", substring);
                                        vel = [substring floatValue] - 1;
                                        NSLog(@"%.2f", vel);
                                    }
                                    else if ([input characterAtIndex:i] == 't')
                                    {
                                        i += 2;
                                        NSRange range;
                                        range.location = i;
                                        range.length = 3;
                                        
                                        NSString *substring = [input substringWithRange:range];
                                        NSLog(@"turn read as %@", substring);
                                        turnAngle = [substring floatValue] - 1;
                                        NSLog(@"%.2f", turnAngle);
                                        
                                    }
                                    else
                                    {
                                        NSLog(@"Invalid symbol");
                                    }
                                }
                            }
                            
                            
                        }
                    }
                }
//NSLog(@"Connect button reset");
               // [self resetConnectButton];
            }
            
            break;
            
        case NSStreamEventErrorOccurred:
            NSLog(@"Can not connect to the host!");
            NSLog(@"Connect button reset");
            [self resetConnectButton];
            break;
            
        case NSStreamEventEndEncountered:
            break;
            
        default:
            NSLog(@"Unknown Event");
    }
    
}

- (void)displayMessage: (NSString *)recievedmessage {
    
    _ConnectionField.text = recievedmessage;
}


//when the user presses connect button
- (IBAction)handleConnectClick:(id)sender {
    NSLog(@"button pressed: %@", [sender currentTitle]);
    //[_connectingButton setTitle: @"Connecting..." forState: UIControlStateNormal];
    _connectingButton.enabled = true;
    [_connectingButton setTitle: @"Connecting..." forState: UIControlStateSelected];
    
    NSThread* netThread;
    [netThread start];
    
    //[self performSelector:@selector(initNetworkCommunication:) onThread:netThread withObject:nil waitUntilDone:true];
    //hangs and doesent connect
    
    [self initNetworkCommunication];
}
- (IBAction)handleStopClick:(id)sender {
    NSLog(@"button pressed: %@", [sender currentTitle]);
    [inputStream close];
    [outputStream close];
    vel = 0.0;
    turnAngle = 0.0;
    [self setIncomeText: @""];
    
    [self resetConnectButton];
}

//when the user presses stop button/*

//set the button to "Connected!"
-(void)setbuttonconnected{
    [_connectingButton setTitle: @"Connected!" forState: UIControlStateNormal];
    [_connectingButton setTitleColor: [UIColor greenColor] forState: UIControlStateNormal];
    _connectingButton.enabled = true;
    _connectingButton.userInteractionEnabled = false;
}

-(void)resetConnectButton{
    [_connectingButton setTitle: @"Connect" forState: UIControlStateNormal];
    [_connectingButton setTitleColor: [UIColor blueColor] forState: UIControlStateNormal];
    _connectingButton.enabled = true;
    _connectingButton.userInteractionEnabled = true;
}

//set the text boxes text
- (void)setIncomeText: (NSString *)txt {
    
    _textField.text = txt;
}

//enable sending information to the server
- (void)enableSending{
    _sendButton.userInteractionEnabled = true;
    _sendField.userInteractionEnabled = true;
}

- (IBAction)sendtoserver:(id)sender {
    NSLog(@"button pressed: %@", [sender currentTitle]);
    NSString *stringToSend = _sendField.text;
    [self SendMessage: stringToSend];
}


@end
