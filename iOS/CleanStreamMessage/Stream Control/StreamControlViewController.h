//
//  StreamControlViewController.h

//

#import <UIKit/UIKit.h>
#import "DoubleControlSDK/DoubleControlSDK.h"

//Set Up 2 Instance Variables, one for input over a network, and one for output
NSInputStream *inputStream;
NSOutputStream *outputStream;


//These are joins on the app screen to control how the individual objects communicate

@interface StreamControlViewController : UIViewController <NSStreamDelegate, DRDoubleDelegate>

//--    Communication Object    --//
@property (weak, nonatomic) IBOutlet UITextField *ConnectionField;
@property (weak, nonatomic) IBOutlet UIButton *connectingButton;
@property (weak, nonatomic) IBOutlet UITextView *textField;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UITextField *sendField;

//--    RTSP Objects    --//
@property (strong, nonatomic) IBOutlet UIView *cameraView;
@property (strong, nonatomic) IBOutlet UILabel *streamAddress;

//--    Double Robot objects    --//
@property (weak, nonatomic) IBOutlet UILabel *DRConnectionStatus;
@property (weak, nonatomic) IBOutlet UILabel *batteryPercentLabel;
@property (weak, nonatomic) IBOutlet UISwitch *DriveSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *rotationSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *kickStandSwitch;
@property (weak, nonatomic) IBOutlet UITextField *serverAddress;
@property (weak, nonatomic) IBOutlet UITextField *serverPort;
@property (weak, nonatomic) IBOutlet UIButton *stopButton;

- (void) startPreview;

@end
