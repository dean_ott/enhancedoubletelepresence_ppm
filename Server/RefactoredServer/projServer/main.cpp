#pragma once
#include "Sock.h"
#include "RobotCommander.h"
#include "ObjectTracker.h"

#include <iostream>

int main()
{
	server::UDPSocket us;
	UDPmsgInfo reading;
	try {
		us = server::UDPSocket(5010);
		std::cout << "Awaiting UltraSound..." << std::endl;
	}
	catch (...)
	{
		std::cout << us.getErrorCode();
	}

	if (!globoRobo)
		globoRobo = new RobotCommander;
		globoRobo->RequestConnect(5009);

	ObjectTracker tracker("rtsp://172.22.27.206/");
	//using same port twice requires set sock opt on both socket

	while (true)
	{
		tracker.findObject();
		tracker.highlightObject();
		tracker.displayFrames();
		//tracker.calculateCommand();

		globoRobo->adjustDrive(tracker.calculateDrive());
		globoRobo->adjustTurn(tracker.calculateTurn());
		
		/*
		try {
			reading = us.sockRecv();
			//std::cout <<"UltraSound data: "<< reading.message << std::endl;
			//tracker.writeText("UltraSoundData: " + reading.message);
			double proximity;
			std::stringstream convert(reading.message);
			if (!(convert >> proximity))
				proximity = 0;//if that fails set proximity to 0

			if (proximity < 35)
			{
				std::cout << "Too close! With: " << proximity << std::endl;
				globoRobo->adjustDrive(0.5); //stop
			}
		}
		catch(...){
			std::cout << "nothing connected to the udp socket" << std::endl;
		}*/
		globoRobo->send();

		if (cv::waitKey(100) >= 0)
			break; 
	}
	return 0;
}
