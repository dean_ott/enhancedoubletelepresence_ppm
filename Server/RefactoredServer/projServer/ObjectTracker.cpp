#include "ObjectTracker.h"

ObjectTracker::ObjectTracker(std::string theStreamAddr)
{
	//vlc arguments
	const char * const vlc_args[] = {
		"-I", "dummy", // Don't use any interface
		"--ignore-config", // Don't use VLC's config
		//"--extraintf=logger", // Log anything -- this will reduce frameloss
		//"--verbose=2", // Be much more verbose then normal for debugging purpose
	};

	vlcInstance = libvlc_new(sizeof(vlc_args) / sizeof(vlc_args[0]), vlc_args);
	
	const char * pathCp = theStreamAddr.c_str();
	media = libvlc_media_new_location(vlcInstance, pathCp);

	mp = libvlc_media_player_new_from_media(media);
	libvlc_media_release(media);
	
	context = (struct ctx*)malloc(sizeof(*context));
	context->mutex = CreateMutex(NULL, FALSE, NULL);

	context->image = new cv::Mat(VIDEO_HEIGHT, VIDEO_WIDTH, CV_8UC3);
	context->pixels = (unsigned char *)context->image->data;
	cv::imshow("Original Image", *context->image);

	libvlc_video_set_callbacks(mp, lock, unlock, display, context);
	libvlc_video_set_format(mp, "RV24", VIDEO_WIDTH, VIDEO_HEIGHT, VIDEO_WIDTH * 24 / 8); // pitch = width * BitsPerPixel / 8

	theObject = { 0, 0, 0 };
	hsvMax = { 256, 256, 256 };
	hsvMin = { 0, 0, 0 };
}

ObjectTracker::ObjectTracker(int theCamID)
{
	cap = cv::VideoCapture(theCamID);

	theObject = { 0, 0, 0 };
	hsvMax = { 256, 256, 256 };
	hsvMin = { 0, 0, 0 };
}

ObjectTracker::~ObjectTracker()
{
	libvlc_media_player_stop(mp);	//destroy stream
}

void ObjectTracker::openStreamLocation()
{
	cap = cv::VideoCapture(streamAddr);
}

void ObjectTracker::displayTrackbars()
{
	cv::namedWindow("HSV Sliders", cv::WINDOW_KEEPRATIO);

	char trackbarName[50];
	// Hue Trackbars
	sprintf_s(trackbarName, "Hue Max", hsvMax.hue);
	cv::createTrackbar(trackbarName, "HSV Sliders", &hsvMax.hue, sliderMax);

	sprintf_s(trackbarName, "Hue Min", hsvMax.hue);
	cv::createTrackbar(trackbarName, "HSV Sliders", &hsvMin.hue, sliderMax);

	// Saturation Trackbars
	sprintf_s(trackbarName, "Saturation Max", hsvMax.saturation);
	cv::createTrackbar(trackbarName, "HSV Sliders", &hsvMax.saturation, sliderMax);

	sprintf_s(trackbarName, "Saturation Min", hsvMax.saturation);
	cv::createTrackbar(trackbarName, "HSV Sliders", &hsvMin.saturation, sliderMax);

	// Value Trackbars
	sprintf_s(trackbarName, "Value Max", hsvMax.value);
	cv::createTrackbar(trackbarName, "HSV Sliders", &hsvMax.value, sliderMax);

	sprintf_s(trackbarName, "Value Min", hsvMax.value);
	cv::createTrackbar(trackbarName, "HSV Sliders", &hsvMin.value, sliderMax);
}

void ObjectTracker::reduceNoise()
{
	cv::blur(binaryFrame, binaryFrame, cv::Size(20, 20));							// Blur image
	cv::threshold(binaryFrame, binaryFrame, 20, 255, cv::THRESH_BINARY);			// Apply a Threshhold
	cv::blur(binaryFrame, binaryFrame, cv::Size(20, 20));							// Blur image
	cv::threshold(binaryFrame, binaryFrame, 20, 255, cv::THRESH_BINARY);			// Apply a Threshhold
}

std::vector<std::vector<cv::Point>> ObjectTracker::findObjectContours()
{
	cv::Mat temp;																						// Temporary image Matrix
	binaryFrame.copyTo(temp);																		// copy the thesholded image to temp
	std::vector<std::vector<cv::Point>> contours;																	// Vector for Contours
	std::vector<cv::Vec4i> hierarchy;																		// Vector for Heirarchy of contours
	cv::findContours(temp, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);				// Find the Contours of the image.
	//cv::imshow("contours", temp);
	if (contours.size() > 0) {																		// check if there are any contours		
		objectDetected = true;																			// if there is, object Detected
	}
	else {
		objectDetected = false;																		// if not, no object detected.
	}
	return contours;
}

void ObjectTracker::findBoundingRect(std::vector<std::vector<cv::Point>> contours)
{
	if (objectDetected) 
	{																							// if there is an object.
		//for (int i = 1; i < contours.size() - 1; i++) {}
		std::vector < std::vector<cv::Point> > largestContourVector;							// Create Vector for the largest contour
		largestContourVector.push_back(contours.at(contours.size() - 1));						// put the biggest contour into 'largestContour'

		objectBoundingRect = boundingRect(largestContourVector.at(0));							// Create a bouning rectangle around the largest contour
		theObject.x = objectBoundingRect.x + objectBoundingRect.width / 2;						// get the Center of the x component
		theObject.y = objectBoundingRect.y + objectBoundingRect.height / 2;						// get the Center of the y component
		theObject.width = objectBoundingRect.width;
		theObject.height = objectBoundingRect.height;
		theObject.rectSize = (objectBoundingRect.width + objectBoundingRect.height) / 2;		// Get the Average of the the width and height
	}
}

void ObjectTracker::isolateObject()
{
	cv::cvtColor(originalFrame, binaryFrame, cv::COLOR_BGR2HSV);
	cv::inRange(binaryFrame, cv::Scalar(hsvMin.hue, hsvMin.saturation, hsvMin.value),
		cv::Scalar(hsvMax.hue, hsvMax.saturation, hsvMax.value), binaryFrame);
	cv::imshow("HSV", binaryFrame);
}

void ObjectTracker::readStream()
{
	//cap.read(originalFrame);
	originalFrame = *context->image;
	
}
void ObjectTracker::rotate(cv::Mat& src, double angle, cv::Mat&dst)
{
	int len = std::max(src.cols, src.rows);
	cv::Point2f pt(len / 2., len / 2.);
	cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);
	cv::warpAffine(src, dst, r, cv::Size(len, len));
}

void ObjectTracker::displayFrames()
{
	if (buffer++ > 5)
	{
		libvlc_media_player_play(mp);
	}
	//originalFrame = *context->image;
	cv::imshow("Original Image", originalFrame);
	//float fps = libvlc_media_player_get_fps(mp);
	//printf("fps:%f\r\n", fps);
}

void ObjectTracker::findObject()
{
	readStream();
	//rotate(originalFrame, 90, originalFrame);
	displayTrackbars();
	isolateObject();
	reduceNoise();
	auto contours = findObjectContours();
	findBoundingRect(contours);
}
void ObjectTracker::writeText(std::string theText)
{
	cv::Vec3b blue = { 255, 0, 0 };
	// Write to the originalframe
	cv::putText(originalFrame, theText, cv::Point(10, 10), 1, 1, blue, 2);
}

void ObjectTracker::highlightObject()
{
	cv::Vec3b red = { 0, 0, 255 };
	// Highlight an object.
	cv::circle(originalFrame, cv::Point(theObject.x, theObject.y), theObject.rectSize, red, 4);										// Draw Circle with a radius based on the size of the bounding rect.
	cv::putText(originalFrame, " (" + ftos(theObject.x) + "," + ftos(theObject.y) + ")",
		cv::Point(theObject.x + 50, theObject.y + 50), 1, 1, red, 2);

	// Put Text on the Screen, converts Int to string.
	// arrowedLine(cameraFeed, Point(x, y), Point(10, 10), blue, 2);									// Draw arrowed lines.
	// arrowedLine(cameraFeed, Point(x, y), Point(630, 10), blue, 2);
	// arrowedLine(cameraFeed, Point(x, y), Point(10, 470), blue, 2);
	// arrowedLine(cameraFeed, Point(x, y), Point(630, 470), blue, 2); 
}

void ObjectTracker::calculateCommand()
{
	cv::Size s = originalFrame.size();

	float d =(theObject.width / s.width); //just base of width
	
	if (d > 0.50)
	{
		//ifobjecttakes up whole screen
		// go backwards
		//no offset
	}
	else if (d < 0.50)
	{
		// if theobject takes up no screen,
		//go forwards
		//no offset
	}
	else {
		d = 1.0; //1.0 stop 2.0 maxforwardspd and 0.0 maxreversespd
	}

	float t =(theObject.x / s.width);
	if (t < s.width/2)
		t += 1;

	robotCommand.drive = d;
}

float ObjectTracker::calculateDrive()
{
	cv::Size s = originalFrame.size();
	float d = (theObject.width / 600); //max like width is 600
	
	std::cout << "Current Driv theObject.width Size: " << theObject.width << std::endl;
	if (d > 0.50)
	{
		//ifobjecttakes up whole screen
		// go backwards
		d = 0.5;
		
	}
	else if (d < 0.50)
	{
		// if theobject takes up no screen,
		//go forwards
		d += 1;
	}
	else {
		d = 0;
	}

	std::cout << "Current Drive: " << d << std::endl;
	return d;
}

float ObjectTracker::calculateTurn()
{
	cv::Size s = originalFrame.size();
	float t = (static_cast<float>(theObject.x) / static_cast<float>(s.height));
	if (t > 0.35 && t < 0.65)
	{
		t = 1.0; //no turn
	}

	std::cout << "Current Turn: " << t << std::endl;
	return t;
}


void ObjectTracker::sendCommand()
{
	calculateCommand();
	//std::string d = ftos(robotCommand.drive);
	//std::string t = ftos(robotCommand.turn);
	//theServer.sendData("-d=" + ftos(robotCommand.drive) + " " + "-t= " + ftos(robotCommand.turn)); //send data is no decominal
}

// non member Function
std::string ftos(float number)
{
	std::stringstream ss(std::stringstream::in | std::stringstream::out);
	// Convert string to integer
	ss.setf(std::ios::fixed);
	ss.precision(1);
	ss << number;
	return ss.str();
}

//Vlc locking

void *lock(void *data, void**p_pixels)
{
	struct ctx *ctx = (struct ctx*)data;

	WaitForSingleObject(ctx->mutex, INFINITE);

	// pixel will be stored on image pixel space
	*p_pixels = ctx->pixels;

	return NULL;

}

void display(void *data, void *id){
	(void)data;
	assert(id == NULL);
}

void unlock(void *data, void *id, void *const *p_pixels){

	// get back data structure 
	struct ctx *ctx = (struct ctx*)data;
	/* VLC just rendered the video, but we can also render stuff */
	// show rendered image
	//cv::imshow("test", *ctx->image);
	ReleaseMutex(ctx->mutex);
}