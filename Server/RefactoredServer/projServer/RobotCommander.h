#pragma once
#include <mutex>
#include <iostream>			//TODO: remvoe later just needed for server debugging
#include <sstream>
#include "Sock.h"

class RobotCommander
{
public:
	RobotCommander()
	{
		//Defaults set to off
		drive = 1.0;
		turn = 1.0;
	};

	void RequestConnect(unsigned int port)
	{
		try {
			std::cout << "Awaiting iOS connection..." << std::endl;
			theServer = server::TCPSocket(port); //5009
		}
		catch (...)
		{
			std::cout << theServer.getErrorCode();
		}
	}

	void adjustDrive(float d)
	{
		robotAccess.lock();
		drive = d;
		robotAccess.unlock();
	}

	void adjustTurn(float t)
	{
		robotAccess.lock();
		turn = t;
		robotAccess.unlock();
	}

	float getTurn()
	{
		return turn;		//thread lock?
	}

	float getDrive()
	{
		return drive; 	//thread lock?
	}

	void send();
private:
	server::TCPSocket theServer;
	std::mutex robotAccess;
	float drive;
	float turn;
};

void RobotCommander::send()
{
	std::stringstream d(std::stringstream::in | std::stringstream::out);
	d.setf(std::ios::fixed);
	d.precision(1);
	d << drive;

	std::stringstream t(std::stringstream::in | std::stringstream::out);
	// Convert string to integer
	t.setf(std::ios::fixed);
	t.precision(1);
	t << drive;

	try {
		theServer.sendData("-d=" + d.str() + " " + "-t=" + t.str());
		//theServer.sendData("-d=" + d.str());
	}
	catch (...)
	{
		std::cout << theServer.getErrorCode();
	}
}


// Default initialization	- I love the name!
RobotCommander *globoRobo = 0;	