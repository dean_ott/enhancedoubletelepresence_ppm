#include "Sock.h"
bool sock::startUpCalled;
WSAData sock::wasaData;

namespace server
{
	TCPSocket::TCPSocket()
	{
		s_ = INVALID_SOCKET;
	}
	UDPSocket::UDPSocket()
	{
		s_ = INVALID_SOCKET;
	}
	UDPSocket::UDPSocket(USHORT theLocalPort)
	{
		localPort = theLocalPort;
		int flag;
		if (!startUpCalled)
		{
			startUp_();
		}

		sockaddr_in info;
		info.sin_family = AF_INET;
		info.sin_port = htons(localPort);
		info.sin_addr.s_addr = INADDR_ANY;//.s_addr =  convertx86("127.0.0.1");
		s_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (s_ == INVALID_SOCKET)
		{
			sockError = WSAGetLastError();
			throw 2;
			//	WSACleanup();
		}

		flag = bind(s_, (SOCKADDR*)&info, sizeof(info));
		if (flag == INVALID_SOCKET)
		{
			sockError = WSAGetLastError();
			closesocket(s_);
			throw 3;
			//	WSACleanup();
		}
		localPort = theLocalPort;
	}
	
	TCPSocket::TCPSocket(unsigned port)
	{
		if (!startUpCalled)
		{
			startUp_();
		}

		s_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (s_ == INVALID_SOCKET)
		{
			sockError = WSAGetLastError();
			throw 1;
		}
		SOCKADDR_IN service;
		service.sin_addr.s_addr = convertx86("0.0.0.0");
		service.sin_family = AF_INET;
		service.sin_port = htons(port);
		int result;
		result = bind(s_, (SOCKADDR*)&service, sizeof(service));
		if (result == SOCKET_ERROR)
		{
			sockError = WSAGetLastError();
			closesocket(s_);
			throw 2;
		}
		listen(s_, SOMAXCONN);
		if (s_ == SOCKET_ERROR)
		{
			sockError = WSAGetLastError();
			throw 3;
		}
		sockaddr_in info; //for holding information about the client
		int addrSize = sizeof(info);
		s_ = accept(s_, (struct sockaddr*)&info, &addrSize);
		if (s_ == INVALID_SOCKET)
		{
			sockError = WSAGetLastError();
			throw 4;
		}
		
		remoteIP.resize(IPLEN);
		const char * x = inet_ntop(AF_INET, &info.sin_addr.s_addr,&remoteIP[0], IPLEN );
		remoteIP = x;
		localPort = port;
		remotePort = ntohs(info.sin_port);
		
	}
}
namespace client
{
	TCPsock::TCPsock(std::string IP, unsigned port)
	{
		s_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (s_ == INVALID_SOCKET)
		{
			sockError = WSAGetLastError();
			throw 2;
		}
		sockaddr_in info;
		info.sin_addr.s_addr = convertx86(IP);
		info.sin_port = htons(port);
		info.sin_family = AF_INET;	
		int result;
		result = connect(s_, (SOCKADDR*)&info, sizeof(info));
		if (result == SOCKET_ERROR)
		{
			sockError = WSAGetLastError();
			closesocket(s_);
			throw 3;
		}
		destIP = IP;
		destPort = port;
		

	}
	UDPSocket::UDPSocket(USHORT theLocalPort)
	{
		localPort = theLocalPort;
		int flag;
		if (!startUpCalled)
		{
			startUp_();
		}

		sockaddr_in info;
		info.sin_family = AF_INET;
		info.sin_port = htons(localPort);
		info.sin_addr.s_addr = INADDR_ANY;//.s_addr =  convertx86("127.0.0.1");
		s_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (s_ == INVALID_SOCKET)
		{
			sockError = WSAGetLastError();
			throw 2;
			//	WSACleanup();
		}

		flag = bind(s_, (SOCKADDR*)&info, sizeof(info));
		if (flag == INVALID_SOCKET)
		{
			sockError = WSAGetLastError();
			closesocket(s_);
			throw 3;
			//	WSACleanup();
		}
		localPort = theLocalPort;
	}
}
std::string TCP::sockRecv()
	{
		std::string buff;
		buff.resize(DEFAULT_BUFLEN);
		int len;
		len = recv(s_, &buff[0], DEFAULT_BUFLEN, NULL);
		if (len == 0)
		{
			sockError = WSAGetLastError();
			throw 5;
		}
		else if (len < 0)
		{
			sockError = WSAGetLastError();
			throw 5;
		}
		buff.resize(len);
		return buff;
	}
void TCP::sendData(std::string data)
	{
		int iSendResult = send(s_, &data[0], data.length(), 0);
		if (iSendResult == SOCKET_ERROR) {
			printf("send failed with error: %d\n", WSAGetLastError());
			closesocket(s_);
			throw 4;
			//WSACleanup() would kill all sockets on the network. Might need to find a way for a particular socket to onyl be closed. Especialyl when we have the two clients.
			//return 1;
		}
	}

UDPmsgInfo UDP::sockRecv()
{
	std::string buff;
	buff.resize(DEFAULT_BUFLEN);
	sockaddr_in info;
	/*info.sin_addr.s_addr = INADDR_ANY;
	info.sin_port = htons(localPort);
	info.sin_family = AF_INET;
	*/
	int sz = sizeof(info);
	int len;
	len = recvfrom(s_, &buff[0], DEFAULT_BUFLEN, NULL, (sockaddr*)&info, &sz);
	if (len == SOCKET_ERROR)
	{
		sockError = WSAGetLastError();
		throw 5;
	}
	buff.resize(len);
	std::string IP;
	IP.resize(IPLEN);
	const char * x = inet_ntop(AF_INET, &info.sin_addr.s_addr, &IP[0], IPLEN);
	
	return (UDPmsgInfo  { buff,IP , ntohs(info.sin_port) });
	/*x.message = buff;
	x.sourceIP = inet_ntoa(info.sin_addr);
	x.sourcePort = ntohs(info.sin_port);*/
}
void UDP::sendData(std::string data, std::string IP, USHORT port)
{
	sockaddr_in info;
	info.sin_addr.s_addr = convertx86(IP);
	info.sin_port = htons(port);
	info.sin_family = AF_INET;
	int len;
	len =sendto(s_, data.c_str(), data.length(), NULL, (SOCKADDR*)&info, sizeof(info));
	if (len == SOCKET_ERROR)
	{
		sockError = WSAGetLastError();
		throw 5;
	}
	
	
}
SOCKET* sock::getSock()
{
	return &s_;
}
int sock::getErrorCode()
{
	return sockError;
}
TCP::TCP()
{

}

sock::sock()
{
	// don't forget me!!!!

}

void sock::startUp_()
	{
		if (!startUpCalled)
		{
			startUpCalled = true;
			int flag = WSAStartup(MAKEWORD(2, 2), &wasaData);
			if (flag != NO_ERROR)
			{
				
				throw 0;
			}
			
		}
	}

UINT32 sock::convertx86(std::string Dotdec)
	{
		int octets[4];
		UINT32 ip = 0;

		char pos = 0;
		char lastPos = 0;

		for (char i = 0; i < 4; i++)
		{
			if (i > 0)
			{
				lastPos++;
			}
			pos = Dotdec.find('.', lastPos);

			octets[i] = stoi(Dotdec.substr(lastPos, (pos - lastPos)));
			lastPos = pos;
		}
		for (char i = 3; i >-1; i--)
		{// stores the bytes in reverse order to compensate for litte endian architecture

			ip += octets[i];
			if (i != 0)
				ip = ip << 8;
		}
		return ip;
	}