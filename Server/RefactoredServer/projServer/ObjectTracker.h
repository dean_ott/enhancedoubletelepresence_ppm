#pragma once
#include <future>
#include <iostream>			//TODO: remvoe later just needed for server debugging
#include "Sock.h"

#include "vlc/libvlc.h"
#include "vlc/libvlc_media.h"
#include "vlc/libvlc_media_player.h"

//#include "opencv2\highgui\highgui.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>
// Members of this are what get sent to iOS app


// define output video resolution
#define VIDEO_WIDTH     900
#define VIDEO_HEIGHT    800

//VLC stream grab setup
struct ctx
{
	cv::Mat* image;
	HANDLE mutex;
	uchar*    pixels;
};

typedef struct robotCommand
{
	float drive;
	float turn;
} ROBOT_COMMAND; 

typedef struct hsvColour
{
	int hue, saturation, value;
} HSV_COLOUR;


typedef struct trackedObject
{
	int x, y;
	float width, height, rectSize;
} TRACKED_OBJECT;

class ObjectTracker
{
public:
	ObjectTracker(std::string streamAddr);
	ObjectTracker(int theCamID);
	~ObjectTracker();
	void findObject();
	void openStreamLocation();
	void displayFrames();
	void highlightObject();

	void calculateCommand();
	float calculateDrive();
	float calculateTurn();
	void sendCommand();
	void readStream();

	void writeText(std::string theText);
	void rotate(cv::Mat& src, double angle, cv::Mat&dst);



private:

	// CV members
	cv::Mat originalFrame;
	cv::Mat binaryFrame;
	cv::VideoCapture cap;
	cv::Rect objectBoundingRect = cv::Rect(0, 0, 0, 0);

	int sliderMax = 256;
	HSV_COLOUR hsvMax;
	HSV_COLOUR hsvMin;

	TRACKED_OBJECT theObject;
	ROBOT_COMMAND robotCommand;

	bool objectDetected = false;

	// Server Members
	std::string streamAddr;
	
	// CV functions
	void displayTrackbars();
	void isolateObject();
	void reduceNoise();
	std::vector<std::vector<cv::Point>> findObjectContours();
	void findBoundingRect(std::vector<std::vector<cv::Point>> contours);
	
	// VLC pointers - privates
	libvlc_instance_t *vlcInstance;
	libvlc_media_player_t *mp;
	libvlc_media_t *media;

	struct ctx* context;

	int buffer = 0;	//i think...
};


std::string ftos(float number);

void unlock(void *data, void *id, void *const *p_pixels);
void display(void *data, void *id);
void *lock(void *data, void**p_pixels);