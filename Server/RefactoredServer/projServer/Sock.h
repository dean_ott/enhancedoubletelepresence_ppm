
#ifdef UNICODE
#undef UNICODE 
#endif
#pragma once
#pragma comment (lib, "Ws2_32.lib")

#include <winsock2.h>	
#include <windows.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>

#define IPLEN 16
#define WIN32_LEAN_AND_MEAN
#define DEFAULT_BUFLEN 1024 

struct UDPmsgInfo
{
	std::string message;
	std::string sourceIP;
	short  sourcePort;
};

class sock
{
protected:
	SOCKET s_;
	static WSAData wasaData;
	static bool startUpCalled;				//why volatile? volatile object - an object whose type is volatile-qualified, or a subobject of a volatile object, or a mutable subobject of a const-volatile object. Every access (read or write operation, member function call, etc.) on the volatile object is treated as a visible side-effect for the purposes of optimization (that is, within a single thread of execution, volatile accesses cannot be reordered or optimized out. This makes volatile objects suitable for communication with a signal handler, but not with another thread of execution, see std::memory_order). Any attempt to refer to a volatile object through a non-volatile glvalue (e.g. through a reference or pointer to non-volatile type) results in undefined behavior.
	static void startUp_();
	static UINT32 convertx86(std::string Dotdec);
	unsigned localPort;

	int sockError;

	
	sock();
public:
	int getErrorCode();
	SOCKET* getSock();
private:
	// this object should not be used use UDPsocket OR TCPsocket
};

class TCP : public sock
{

protected:
	TCP();
	unsigned remotePort;
	std::string remoteIP;
public:
	std::string sockRecv();
	void sendData(std::string data);
};

class UDP : public sock
{
public:
	UDPmsgInfo sockRecv();
	void sendData(std::string data, std::string IP, unsigned short port);
};

namespace server
{
	class UDPSocket : public UDP
	{
	public:
		UDPSocket(USHORT theLocalPort);
		UDPSocket();
	};

	class TCPSocket :public TCP
	{
	public:
		TCPSocket(unsigned port);
		TCPSocket();
	};
}

namespace client
{
	class TCPsock : TCP
	{
	private:
		unsigned destPort;
		std::string destIP;
	public:
		TCPsock(std::string IP, unsigned port);
	};

	class UDPSocket :UDP
	{
	public:
		UDPSocket(USHORT thelocalPort);

	};
}

