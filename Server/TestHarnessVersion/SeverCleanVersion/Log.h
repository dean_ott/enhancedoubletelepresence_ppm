///////////////////////////////////////////////////
//DEAN OTTEWELL LOGGER COURSEWORK 1 SDI N0568745//
/////////////////////////////////////////////////

#ifndef LOG_H_
#define LOG_H_


#include <string>
#include <sstream>
#include <iostream>
#include <fstream>

#include <cstdarg>
#include <ctype.h>

#include <typeinfo>

#include <mutex>			//Thread safety

#include "CLargs.h"

//Log to config streams
#define LOG(lvl) \
	SDI::Logger().get(lvl) \

//Adjust the logger save file path.
#define LOGFILECONFIG(path) \
	SDI::StreamOutput::filePathSet(path) \

// Adjust the logger stream path.
#define LOGSTREAMCONFIG(stream) \
	SDI::StreamOutput::set_stream(stream) \

//List of serverity logAll, logErr, logWarn, logInfo, logDebug, logNone 
enum Severity { logAll, logErr, logWarn, logInfo, logDebug, logNone };

//TODO: 
//NO DEBUG CATCH


namespace SDI
{
	//Thread locker		-	global instance and as mutliple instances of log are possible
	static std::mutex logMtx;

	template <typename policy>
	class Log						
	{
		public:
			Log();
			Log(int argc, char* argv[]);
			~Log();
			std::ostringstream& get(Severity lvl); 

			static Severity& loggingLvl();

		private:
			Severity messLvl;
			std::ostringstream out;
	};

	class StreamOutput	//general stream and file streams
	{
		public:
			static void filePathSet(std::string path);	
			static void output(const std::string& msg);			//Outputs, sets default. Can have no filepath cant have no out written
			static void set_stream(std::ostream& stream)
			{
				stream_ = &stream;
			}
			static std::ostream& streamInstance() 
			{
				return *stream_;
			}

			static void stopFile()
			{
				pathName.clear();
			}
			static void stopStream()
			{
				stream_ = NULL;
			}

		private:
			static std::ostream* stream_;
			static std::string pathName;
	};

	class Logger : public Log < SDI::StreamOutput > {};

	/*Non member functions*/ 
	template<typename T>
	std::string  collapse(T v);

	template<typename T, typename... Args>
	std::string collapse(T first, Args... args);

	template<typename T>
	std::string collDetails(T v);
}

std::ostream* SDI::StreamOutput::stream_ = NULL;
std::string SDI::StreamOutput::pathName = "";

#include "Log.cpp"

#endif /*LOG_H_*/ 
