#ifndef CAP_H_
#define CAP_H_

//#include "Log.h"			-		issue with mutex recreation
//#include "opencv2/highgui/highgui.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>

#include <vector>
#include <string>

namespace DV
{
	class Capture
	{
		public:
			Capture(std::string video);
			~Capture();
			int getDetectionInfo();			//check return
			Capture();

		private:
			void BeginStream();				//Loop for frame capture
		
			//Frame analysis//
			void createTrackbars();
			void isolateObject(cv::Mat &binaryFrame, cv::Mat  originalFrame);
			void findObject(cv::Mat  binaryFrame, cv::Mat  &originalFrame);
			void reduceNoise(cv::Mat  &originalFrame);

			//Private varaibles//
			cv::VideoCapture cap_;
			std::vector<cv::Mat> storeFrames_;
			std::string windowName_ = "Video Capture Window: ";
			cv::Mat frame_;					//store image here of sad face no stream
			bool readSuccess;
			int delay_ = 1;					//defaulted15 ms between frame acquisitions
	
			// HSV Min Max variables
			const int sliderMax_ = 256;
			int hMin_ = 0;
			int sMin_ = 0;
			int vMin_ = 0;

			int hMax_ = 256;
			int sMax_ = 256;
			int vMax_ = 256;

			cv::Rect objectBoundingRectangle = cv::Rect(0, 0, 0, 0);		//Is cv:: the old one?
			//int theObject[2] = { 0, 0 };		NEW CONTAINER
			int avgBoundingSize = 0;
			//int frameStartPixel[2] = { 0, 0 };
			//int frameEndPixel[2] = { 640, 480 };

	};
}
#endif /*CAP_H_*/
