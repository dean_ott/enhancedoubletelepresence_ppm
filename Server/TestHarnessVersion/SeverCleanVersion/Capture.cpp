#ifndef CAP_CPP_
#define CAP_CPP_

#include "Capture.h"


namespace DV
{
	Capture::Capture()
	{

	}

	Capture::Capture(std::string video)
	{
		cv::VideoCapture cap(video);	//The passed in rtsp would need to be formated
		cap_ = cap;

		windowName_ += video;	//Name window fittingly 

		if (!cap.isOpened()) //exits the program if no success
		{
			//LOG(logErr) << "Cannot open the video file" << std::endl;
		}
		else
		{
			//BeginStream();
		}
	}

	void Capture::BeginStream()
	{
		std::string windowName;

		//LOG(logWarn) << "The window name " << windowName << "has been created.";

		cv::namedWindow(windowName_, CV_WINDOW_AUTOSIZE); // creates a window called "myvideo"

		while (1)
		{
			bool bSuccess = cap_.read(frame_); //read new a new frame from video



			if (!bSuccess) //if not success break loops
			{
				//LOG(logErr) << "Cannot read the frame from video file" << std::endl;
				break;
			}

			cv::imshow(windowName_, frame_); //show the frame in "MyVideo" window

			//storeFrames_.push_back(frame_);

			if (cv::waitKey(delay_) == 27) //wait for 'esc' key press for 30ms. if 'esc key is pressed , breaks loop
			{
				//LOG(logWarn) << "Esc key is pressed by user" << std::endl;
				//LOGFILECONFIG("imagesetrawform.txt");
				//LOG(logInfo) << storeFrames_; how about noo
				break;
			}
		}
	}
	
	void Capture::createTrackbars() 
	{
		cv::namedWindow(windowName_, cv::WINDOW_KEEPRATIO);

		// NOTE: update iterate through arrays that contain titles, max and sliders...
		char trackbarName[50];

		sprintf(trackbarName, "Hue Max", hMax_);
		cv::createTrackbar(trackbarName, "HSV Sliders", &hMax_, sliderMax_);

		sprintf(trackbarName, "Hue Min", hMax_);
		cv::createTrackbar(trackbarName, "HSV Sliders", &hMin_, sliderMax_);

		sprintf(trackbarName, "Saturation Max", hMax_);
		cv::createTrackbar(trackbarName, "HSV Sliders", &sMin_, sliderMax_);

		sprintf(trackbarName, "Saturation Min", sMax_);
		cv::createTrackbar(trackbarName, "HSV Sliders", &sMin_, sliderMax_);

		sprintf(trackbarName, "Value Max", vMax_);
		cv::createTrackbar(trackbarName, "HSV Sliders", &vMin_, sliderMax_);

		sprintf(trackbarName, "Value Min", vMax_);
		cv::createTrackbar(trackbarName, "HSV Sliders", &vMin_, sliderMax_);
	}

	void Capture::isolateObject(cv::Mat &binaryFrame, cv::Mat  originalFrame)
	{
		cv::cvtColor(originalFrame, binaryFrame, cv::COLOR_BGR2HSV);
		// imshow("HSV", binFrame);
		inRange(binaryFrame, cv::Scalar(hMin_, sMin_, vMin_), cv::Scalar(hMax_, sMax_, vMax_), binaryFrame);
	}

	void Capture::findObject(cv::Mat  binaryFrame, cv::Mat  &originalFrame)
	{

	}

	void Capture::reduceNoise(cv::Mat  &originalFrame)
	{

	}

	Capture::~Capture()
	{
	}
}
#endif /*CAP_CPP_*/