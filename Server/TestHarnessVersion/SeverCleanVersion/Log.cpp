///////////////////////////////////////////////////
//DEAN OTTEWELL LOGGER COURSEWORK 1 SDI N0568745//
/////////////////////////////////////////////////

#ifndef LOG_CPP_
#define LOG_CPP_

#include "Log.h"

namespace SDI 
{
	void StreamOutput::output(const std::string& msg)
	{
		if (pathName.empty() && stream_ == NULL)
		{
			SDI::StreamOutput::filePathSet("errFile.txt");
			SDI::StreamOutput::set_stream(std::cout); 
			LOG(logWarn) << "No log stream was chosen defaulted to stdcout and root errFile.txt";
		}

		if (!pathName.empty())
		{
			std::ofstream ofs;
			ofs.open(pathName, std::ofstream::out | std::ofstream::app);
			ofs << msg << std::endl;
			ofs.close();
		}
		
		if (stream_ != NULL)
		{
			streamInstance() << msg << std::endl;
		}
	}

	void StreamOutput::filePathSet(std::string path)
	{
		pathName = path;
		std::ofstream ofs;
		ofs.open(pathName, std::ofstream::out | std::ofstream::app);
		ofs << "Log Path Access Configgered at " << __TIME__ << std::endl;
		ofs << "Loci of access " << __FILE__ << std::endl;
		ofs.close();
	}

	/*Non member functions*/ //how do i now get looging level?
	template<typename T>
	std::string  collapse(T v)
	{
		return collDetails(v);
	}

	template<typename T, typename... Args>
	std::string collapse(T first, Args... args) 
	{
		return collDetails(first) + collapse(args...);
	}

	template<typename T>
	std::string collDetails(T v)
	{
		std::stringstream data;

		data << " Variable Value: " << v;

		if (SDI::Logger().loggingLvl() < logInfo)
		{
			data << " Datatype: " << typeid(v).name();

			T* vptr = &v;
			data << " Current Mem Address: " << vptr;
		}

		data << std::endl;
		return data.str();
	}


	template <typename policy>
	std::ostringstream& Log<policy>::get(Severity lvl /*= */)
	{
		out << "-Time " << __TIME__;
		out << "-Logging Level " + std::to_string(lvl) << ": ";
		messLvl = lvl;
		return out;
	}

	template <typename policy>
	Log<policy>::Log()
	{
	}

	template <typename policy>
	Log<policy>::Log(int argc, char* argv[])
	{
		auto vals = SDI::parseArgValues(argc, argv);

		for (auto it = vals.begin(); it != vals.end(); ++it)
		{
			switch (toupper(it->first))
			{
			case 'F':
				SDI::StreamOutput::filePathSet(it->second);
				break;
			default:
				LOG(logWarn) << "No path chosen defaults will occur.";
				break;
			}
		}

		auto flags = SDI::parseArgsFlags(argc, argv);
	
		for (auto it = flags.begin(); it != flags.end(); ++it)
		{
			switch (toupper(*it))
			{
				case 'D':
					SDI::Logger().loggingLvl() = logDebug;
					break;
				case ('N') :
					SDI::Logger().loggingLvl() = logNone;
					break;
				case ('E'):
					SDI::Logger().loggingLvl() = logErr;
					break;
				case ('W'):
					SDI::Logger().loggingLvl() = logWarn;
					break;
				case ('I'):
					SDI::Logger().loggingLvl() = logInfo;
					break;
				case ('A') :
					Log::loggingLvl() = logAll;
					break;
				default:
					LOG(logWarn) << "Unkown logging level: " << *it << "'. Using INFO level as default." << std::endl;
					break;
			}
		}
	
	}

	template <typename policy>
	Severity& Log<policy>::loggingLvl()
	{
		static Severity logLvl = logDebug; //Default but can reassign the static return;
		return logLvl;
	}
	
	template <typename policy>
	Log<policy>::~Log()
	{
		SDI::logMtx.lock();		//Wait for lock to output nicely
		if (messLvl >= Log::loggingLvl())
		{
			policy::output(out.str());
			//policy::instance()->output(out.str());
		}
		SDI::logMtx.unlock();
	}

}
#endif /*LOG_CPP_*/