#include "Sender.h"

Sender::Sender(char sendbuf[], SOCKET ClientSocket)
{
	sendbuf_ = sendbuf;
	ClientSocket_ = ClientSocket;
	std::cout << "Sending Data Launched..." << std::endl;
	SendMessageControl();
}

void Sender::SendMessageControl()
{
	do 
	{
		forwardData = DataReceive();
		messageLength_ = forwardData.size();

		for (int i = 0; i < messageLength_; i++)
		{
			sendbuf[i] = sendMessage_[i];
		}

		sendResult_ = send(ClientSocket_, sendbuf, messageLength_, 0);
		if (sendResult_ == SOCKET_ERROR) {
			printf("send failed with error: %d\n", WSAGetLastError());
			closesocket(ClientSocket_);
			//WSACleanup() would kill all sockets on the network. Might need to find a way for a particular socket to onyl be closed. Especialyl when we have the two clients.
			//return 1;
		}
		printf("Bytes sent: %d\n", sendResult_);

	} while (sendResult_ > 0);
}

std::string Sender::DataReceive()
{
	std::string data;
	std::cout << "Please Enter a message: \n";
	std::getline(std::cin, data);
	return data;
}

Sender::~Sender()
{
}
