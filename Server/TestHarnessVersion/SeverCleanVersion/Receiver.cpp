#include "Receiver.h"


Receiver::Receiver()
{

	//Declare recbuf in here.
	std::cout << "Recieving Data Thread Launched..." << std::endl;
	SendMessageControl();
}

Receiver::ReceiveMessageControl()
{
	do {
		//ignoring the recieving check for now
		iResult = recv(ClientSocket, recvbuf, recvbuflen, 0);

		if (iResult > 0) {
			//printf("Bytes received: %d\n", iResult);
			std::cout << "Message Recieved: ";// << message << std::endl;
			for (int i = 0; i < iResult; i++)
			{
				std::cout << recvbuf[i];
			} // Better syntax to snap what i need. Need to convert to string. std::string.substr(0, iResult);
			std::cout << std::endl;
		}
		else if (iResult == 0)
			printf("Connection closing...\n");
		else  {
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(ClientSocket);
			WSACleanup();
			//return 1;
		}

	} while (iResult != 0);
}

Receiver::~Receiver()
{
}
