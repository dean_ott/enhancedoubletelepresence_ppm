#include "Log.h"

#undef UNICODE
#define WIN32_LEAN_AND_MEAN	//We nee this is winsock2 whines :/ - note we never us it in source...
#define DEFAULT_BUFLEN 1024 //Buffer length is the size of the string info we can listen for in one receive
#define DEFAULT_PORT "5009" //Change the port here - dont really need to change port but could do with making configourable

#include "opencv2/highgui/highgui.hpp"

#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

#include "Capture.h"

//Threading libs
#include <future> 

//Sockets libs
//#include <windows.h>		- we dont need it?
#include <winsock2.h>	
#include <ws2tcpip.h>

//Thread safety
#include <mutex>

// Needed to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")

enum CONTROLS  {STOP = 0, FORWARD = 1, BACKWARD = 2, LEFT = 3, RIGHT = 4}; //Standard enum set to reference.

int direct = STOP;			//I did the directin global cus testing
int getDirection(){			//But but look i have a getter for no reason ;)
	return direct;			//Seriously though this as the access later would be similar to this once class structure created 
}							//Didnt anticipate so many issues with depdencies though

//NETWORK CALL FUNCTIONS//

//Stream//
void ipadVideo(std::string ipadAddr);					//Primairly sets up opencv and then calls BeginStream
void BeginStream(cv::VideoCapture cap);		//Gets capture

//Messenger(Used for ipad control send though//
void sendingData(char sendbuf[], SOCKET ClientSocket);
void receivingData(char recvbuf[], int recvbuflen, SOCKET ClientSocket);

int iResult;				//Kinda like a status checker for winsock. 
							//WIth this we can determine the status of socket functions and compare against it
WSADATA wsaData;			//it gets called but didnt know purpose

int main(int argc, char * argv[])
{
	std::async(ipadVideo, "rtsp://172.22.24.203/");

	//Config log through command line
	SDI::Logger::Log(argc, argv);
	
	LOG(logDebug) << "Log was created";

	//Server to listen to whats being connected
	SOCKET ListenSocket = INVALID_SOCKET;

	//This is the clients socket connection to be used 
	//is the accepted connection the server decides on 
	SOCKET ClientSocket = INVALID_SOCKET;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	//Constraints on data send and retrieveal//
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;
	char sendbuf[DEFAULT_BUFLEN];
	int sendbuflen = DEFAULT_BUFLEN;


	LOG(logInfo) << "Network constraints with the setup " << SDI::collapse(recvbuf, recvbuflen, recvbuf, sendbuflen);

	// Initialize Winsock
	int flag = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (flag != NO_ERROR)
	{
		LOG(logErr) << "Error at WSAStartup()\n";
	}

	//Establish retrival of rasp US data
	//std::async(raspPi);

	ZeroMemory(&hints, sizeof(hints));				//sets struct bit data to 0

	hints.ai_family = AF_INET;						//IPv4 address family
	hints.ai_socktype = SOCK_STREAM;				// specific stream socket 
	hints.ai_protocol = IPPROTO_TCP;				//TCP protocol
	hints.ai_flags = AI_PASSIVE;					

	LOG(logInfo) << "Network configured with the setup " << SDI::collapse(hints.ai_family, hints.ai_socktype, hints.ai_protocol, hints.ai_flags);
	//SDI::collapse(hints.ai_family, hints.ai_socktype, hints.ai_protocol, hints.ai_flags);
	
	LOGSTREAMCONFIG(std::cout);

	// Resolve the server address and port 
	//Check if setting are possible
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		LOG(logErr) << ("getaddrinfo failed with error: %d\n", iResult);
		//WSACleanup();					//<--this could break things
		return 1;
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		LOG(logErr) << ("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		//WSACleanup();					//<--this could break things
		return 1;
	}

	// Setup the TCP listening socket // -
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		LOG(logErr) << ("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		//WSACleanup();					//<--this could break things
		return 1;
	}

	freeaddrinfo(result);

	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		LOG(logErr) << ("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		//WSACleanup();				//<--this could break things
		return 1;
	}

	/*char ac[80];
	if (gethostname(ac, sizeof(ac)) == SOCKET_ERROR) {
		LOG(logErr) << "Error " << WSAGetLastError() << " when getting local host name.";
		return 1;
	}
	
	LOG(logInfo) << "Host name is " << ac;

	struct hostent *phe = gethostbyname(ac);
	if (phe == 0) {
		LOG(logErr) << "Yow! Bad host lookup.";
		return 1;
	}

	for (int i = 0; phe->h_addr_list[i] != 0; ++i) {
		struct in_addr addr;
		memcpy(&addr, phe->h_addr_list[i], sizeof(struct in_addr));
		LOG(logInfo) << "Host address " << i << ": " << inet_ntoa(addr);
	}
*/
	// Waits to accept a client socket - this works with anything really so basically anyone on network could connect to this.
	//They just need ip. The current ipad version we hard code in. Two people should create a input for this.
	ClientSocket = accept(ListenSocket, NULL, NULL);
	if (ClientSocket == INVALID_SOCKET) {
		LOG(logErr) << ("accept failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		//WSACleanup();				//<--this could break things
		return 1;
	}

	// No longer need server listening on the socket - we have  a connection
	LOG(logWarn) << "Listren socket is about to close. Details: " << SDI::collapse(ListenSocket);
	closesocket(ListenSocket);


	SDI::StreamOutput::stopStream();//stop the console output as we need this for log messages
	LOGFILECONFIG("communicationLog.txt");

	//std::async(sendingData, sendbuf, ClientSocket);
	//std::async(receivingData, recvbuf, recvbuflen, ClientSocket);

	//Establish stream retieval through RTSP - 

	//keep stalling the end until a message for disconnect or loss of connecton happens
	//we should really pause this until something happens
	while (true)
	{
		//use iResult better and change before we check it
		bool stopAll = false;
		if (stopAll == true)
		{
			break;
		}
	}

	// shutdown the connection since we're done
	iResult = shutdown(ClientSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		LOG(logErr) << ("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
		return 1;
	}

	// Cleanup
	LOG(logWarn) << "Closing socket " << SDI::collapse(ClientSocket);
	closesocket(ClientSocket);
	//must syncronise  threads
	LOG(logWarn) << "WSACleanup was called";
	WSACleanup();
	//
	return 0;
}

void ipadVideo(std::string ipadAddr)
{
	LOG(logInfo) << "Intilzing ipad connection";

	DV::Capture streamGrab(ipadAddr);

	LOG(logWarn) << "The end of the stream";
}

void sendingData(char sendbuf[], SOCKET ClientSocket)
{
	LOG(logWarn) << "Sending Data Thread Launched..." << std::endl;

	//need to really put isendresutl out
	//basically we have to setif it changes to 0 we end sending
	int iSendResult;

	do {
		std::string sendMessage;
		std::cout << "Please Enter a message: \n";					//Change this to sendMessage = getDirection ();
		std::getline(std::cin, sendMessage);

		//sendMessage = std::to_string(getDirection());

		int messageLength = sendMessage.size();

		for (int i = 0; i < messageLength; i++)
		{
			sendbuf[i] = sendMessage[i];
		}

		LOG(logInfo) << ("Message going to send: %d", sendbuf);
		// Echo the buffer back to the sender
		//Sending a message everytime i get something 

		iSendResult = send(ClientSocket, sendbuf, messageLength, 0);
		if (iSendResult == SOCKET_ERROR) {
			LOG(logErr) << ("send failed with error: %d\n", WSAGetLastError());
			closesocket(ClientSocket);
			//WSACleanup() would kill all sockets on the network. Might need to find a way for a particular socket to onyl be closed. Especialyl when we have the two clients.
			//return 1;
		}
		LOG(logInfo) << ("Bytes sent: %d\n", iSendResult);

	} while (iSendResult > 0); 
	SDI::Logger::loggingLvl() = logWarn;
}

void receivingData(char recvbuf[], int recvbuflen, SOCKET ClientSocket)
{
	//Declare recbuf in here.
	LOG(logWarn) << "Recieving Data Thread Launched..." << std::endl;
	
	do {
		//ignoring the recieving check for now
		iResult = recv(ClientSocket, recvbuf, recvbuflen, 0);

		if (iResult > 0) {
			LOG(logInfo) << "Bytes Recieved: " << iResult;// << message << std::endl;

			std::string message = "";
			for (int i = 0; i < iResult; ++i)
			{
				message.push_back(recvbuf[i]);
			} // Better syntax to snap what i need. Need to convert to string. 
		
			LOG(logInfo) << "The message received :" << message;			
		}		
		else  {
			LOG(logErr) << ("recv failed with error: %d\n", WSAGetLastError());
			closesocket(ClientSocket);
			WSACleanup();
			//return 1;
		}

	} while (iResult > 0);
	LOG(logErr) << ("Connection closing...\n");
	SDI::Logger::loggingLvl() = logWarn;
}

//--		OPENCV STRAIGHT TEST		--// --This was here because capture header and cpp screwed . basically these go in capture but then need to share a result
/*
void isolateObject(cv::Mat  &binFrame, cv::Mat  orgFrame) {
cv::cvtColor(orgFrame, binFrame, cv::COLOR_BGR2HSV);
// imshow("HSV", binFrame);
inRange(binFrame, cv::Scalar(hMin, sMin, vMin), Scalar(hMax, sMax, vMax), binFrame); // research Scalar Class
}

void createTrackbars() {
cv::namedWindow("HSV Sliders", cv::WINDOW_KEEPRATIO);

// NOTE: update iterate through arrays that contain titles, max and sliders...
char trackbarName[50];
sprintf(trackbarName, "Hue Max", hMax);
cv::createTrackbar(trackbarName, "HSV Sliders", &hMax, sliderMax);
sprintf(trackbarName, "Hue Min", hMax);
cv::createTrackbar(trackbarName, "HSV Sliders", &hMin, sliderMax);
sprintf(trackbarName, "Saturation Max", hMax);
cv::createTrackbar(trackbarName, "HSV Sliders", &sMax, sliderMax);
sprintf(trackbarName, "Saturation Min", hMax);
cv::createTrackbar(trackbarName, "HSV Sliders", &sMin, sliderMax);
sprintf(trackbarName, "Value Max", vMax);
cv::createTrackbar(trackbarName, "HSV Sliders", &vMax, sliderMax);
sprintf(trackbarName, "Value Min", vMax);
cv::createTrackbar(trackbarName, "HSV Sliders", &vMin, sliderMax);
}

void reduceNoise(cv::Mat  &binaryFrame) {
// Generates a well Defined Image binary by reducing Noise
// threshold(binaryFrame, binaryFrame, 20, 255, THRESH_BINARY);			// apply a Threshhold

blur(binaryFrame, binaryFrame, Size(20, 20));							// Blur image
threshold(binaryFrame, binaryFrame, 20, 255, THRESH_BINARY);			// Apply a Threshhold
blur(binaryFrame, binaryFrame, Size(20, 20));							// Blur image
threshold(binaryFrame, binaryFrame, 20, 255, THRESH_BINARY);			// Apply a Threshhold

//something cool, don't work well with above
Canny(binaryFrame, binaryFrame, 100, 255, 3);
}

std::string itos(int number)
{
// Convert string to integer
std::stringstream ss;
ss << number;
return ss.str();
}

void highlightObject(cv::Mat  &cameraFeed, int x, int y)
{
// Highlight an object.
cv::circle(cameraFeed, cv::Point(x, y), avgBoundingSize, cv::red, 4);										// Draw Circle with a radius based on the size of the bounding rect.
cv::putText(cameraFeed, " (" + itos(x) + "," + itos(y) + ")",
cv::Point(x + 50, y + 50), 1, 1, cv::red, 2);														// Put Text on the Screen, converts Int to string.
// arrowedLine(cameraFeed, Point(x, y), Point(10, 10), blue, 2);									// Draw arrowed lines.
// arrowedLine(cameraFeed, Point(x, y), Point(630, 10), blue, 2);
// arrowedLine(cameraFeed, Point(x, y), Point(10, 470), blue, 2);
// arrowedLine(cameraFeed, Point(x, y), Point(630, 470), blue, 2);
}

void findObject(cv::Mat  threshImage, cv::Mat  &cameraFeed){
bool objDetected = false;																		// Bool for checking of an object is detected.
cv::Mat temp;																						// Temporary image Matrix
threshImage.copyTo(temp);																		// copy the thesholded image to temp
std::vector<std::vector<cv::Point>> contours;																	// Vector for Contours
std::vector<Vec4i> hierarchy;																		// Vector for Heirarchy of contours
cv::findContours(temp, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);				// Find the Contours of the image.
imshow("contours", temp);
if (contours.size() > 0) {																		// check if there are any contours
objDetected = true;																			// if there is, object Detected
}
else {
objDetected = false;																		// if not, no object detected.
}

// imshow("contours", temp);																	// Display Temp image of contours

if (objDetected) {																				// if there is an object.
//for (int i = 1; i < contours.size() - 1; i++) {
std::vector < std::vector<cv::Point> > largestContourVector;												// Create Vector for the largest contour
largestContourVector.push_back(contours.at(contours.size() - 1));							// put the biggest contour into 'largestContour'

objectBoundingRectangle = cv::boundingRect(largestContourVector.at(0));							// Create a bouning rectangle around the largest contour
int xpos = objectBoundingRectangle.x + objectBoundingRectangle.width / 2;					// get the Center of the x component
int ypos = objectBoundingRectangle.y + objectBoundingRectangle.height / 2;					// get the Center of the y component
avgBoundingSize = (objectBoundingRectangle.width + objectBoundingRectangle.height) / 2;		// Get the Average of the the width and height
theObject[0] = xpos;																		// Store the central x Position
theObject[1] = ypos;																		// store the central y position

highlightObject(cameraFeed, theObject[0], theObject[1]);										// highlight the object on the original image.
}
}*/