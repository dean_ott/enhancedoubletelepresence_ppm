#pragma  once
#include <winsock2.h>	
#include <iostream>
#include <istream>

class Sender
{
private:
	int sendResult_;
	int messageLength_;
	SOCKET ClientSocket_;
	std::string forwardData;
	char sendbuf_[];

public:
	Sender(char sendbuf[], SOCKET ClientSocket);
	void SendMessageControl();
	std::string DataReceive();
	~Sender();
};

