#ifndef CLargs_H_
#define Clargs_H_

#include <string>
#include <vector>
#include <set>
#include <map>

namespace SDI {

	std::vector<std::string> parseArgs(int argc, char * argv[]);

	std::set<char> parseArgsFlags(int argc, char * argv[]);

	std::map<char, std::string> parseArgValues(int argc, char * argv[]);
}
#endif /*Clargs_H_*/