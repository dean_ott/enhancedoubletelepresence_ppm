#ifndef CLargs_CPP_
#define Clargs_CPP_

#include "CLargs.h"

namespace SDI {

	std::vector<std::string> parseArgs(int argc, char * argv[])
	{
		std::vector<std::string> converted;

		for (int i = 1; i < argc; ++i)		//start 1 as 1 reserved
		{
			converted.push_back(argv[i]);
		}

		return converted;
	}

	std::set<char> parseArgsFlags(int argc, char * argv[])
	{
		std::set<char> flags;

		auto strArgs = parseArgs(argc, argv);

		for (int i = 0; i < strArgs.size(); ++i)
		{
			if ((strArgs[i][0] == '-' || strArgs[i][0] == '/'))
			{
				if (strArgs[i].size() == 2)
				{
					flags.insert(strArgs[i][1]);
				}
				else
				{
					for (int j = 1; j < strArgs[i].length() ; ++j) //allows for countinous flags e.g. -abdhdn
					{
						if (strArgs[i][1 + j] == '=')
						{
							j = strArgs[i].length();
						}
						else
						{
							flags.insert(strArgs[i][j]);
						}
					}
				}
			}
		}

		return flags;
	}

	std::map<char, std::string> parseArgValues(int argc, char * argv[])
	{
		std::map<char, std::string> reel;

		auto strArgs = parseArgs(argc, argv);

		for (int i = 0; i < strArgs.size(); ++i)
		{
			for (int j = 0; j < strArgs[i].size(); ++j)
			{
				if (strArgs[i][j] == '=')
				{

					std::string message = "";
					char ticket = strArgs[i][j - 1];

					while (j < strArgs[i].size())
					{
						message.push_back(strArgs[i][++j]);
					}

					reel.insert(std::make_pair(ticket, message));
				}
			}
		}
		return reel;
	}
}
#endif /*Clargs_CPP */
