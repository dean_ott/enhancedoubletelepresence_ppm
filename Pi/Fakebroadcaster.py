# Send UDP broadcast packets
import socket, sys, time

UDP_PORT = 5010
UDP_IP = "127.0.0.1"

print ("UDP target IP:", UDP_IP)
print ("UDP target port:", UDP_PORT)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while 1:
    data = repr(time.time()) + '\n'
    print(data)
    sock.sendto(bytes(data + "\n", "utf-8"), (UDP_IP, UDP_PORT))
