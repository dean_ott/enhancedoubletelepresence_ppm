# Send UDP broadcast packets
import RPi.GPIO as GPIO
import socket, sys, time

GPIO.setmode(GPIO.BCM)

TRIG = 23

ECHO = 24

print("Mesureing distance")

GPIO.setup(TRIG,GPIO.OUT)
GPIO.setup(ECHO,GPIO.IN)

GPIO.output(TRIG,False)
print("Waiting for sensor to settle")
time.sleep(2)

UDP_PORT = 5010
UDP_IP = "127.0.0.1"

print ("UDP target IP:", UDP_IP)
print ("UDP target port:", UDP_PORT)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while 1:
    GPIO.output(TRIG,True)
    time.sleep(0.00001)
    GPIO.output(TRIG, False)

    while GPIO.input(ECHO)==0:
        pulse_start = time.time()
    while GPIO.input(ECHO)==1:
        pulse_end = time.time()

    pulse_duration = pulse_end - pulse_start

    distance = pulse_duration * 17150

    distance = round(distance, 2)

    print("Distance: ", distance, "cm"

GPIO.cleanup()
    
##    data = repr(time.time()) + '\n'
##    print(data)
##    sock.sendto(bytes(data + "\n", "utf-8"), (UDP_IP, UDP_PORT))
